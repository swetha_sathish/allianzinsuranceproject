package Insurance;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import constants.Constants;
import pom.Pom;
import utility.ExcelUtils;
import utility.UpdateExcel;


public class Allianz {

	public static WebDriver driver;
	static WebDriverWait wait;
	static ExtentHtmlReporter htmlReporter;
	static ExtentReports extent;
	static ExtentTest logger;
	static String name="";
	static String year="";
	static String dob="";
	static String age="";
	@BeforeSuite
	public static void launch_url() throws Exception {
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") +"/test-output/AllinazNextReport.html");
		extent = new ExtentReports ();
		ExtentHtmlReporter emailReporter = new ExtentHtmlReporter("email.html");
		

		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("browser name","chrome");
		extent.setSystemInfo("Host Name", "Allianz");
		extent.setSystemInfo("Environment", "Automation Testing");
		extent.setSystemInfo("User Name", "Swetha");
		
		htmlReporter.config().setDocumentTitle("Allianz");
		htmlReporter.config().setReportName("Automation testing");
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.DARK);		
		htmlReporter.loadXMLConfig(new File(System.getProperty("user.dir") +"\\extent-config.xml"));
		extent.attachReporter(emailReporter);
		

		System.setProperty("webdriver.chrome.driver", "E:\\Jars\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 30);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(Constants.Allianaz_Url);
		
	}

	@Test(priority=1)
	public static void car_insurance() throws Exception {	
		
		WebElement car_insurance=Pom.Car_insurance(driver);
		Actions a=new Actions(driver);
		a.moveToElement(car_insurance).build().perform();
		logger = extent.createTest("Insurance", "verify the login page");
	    logger.log(Status.PASS, MarkupHelper.createLabel("Test Case Passed is passTest", ExtentColor.GREEN));
		WebElement get_quote=wait.until(ExpectedConditions.elementToBeClickable(Pom.Get_quote(driver)));
	    get_quote.click();
	    for (String handle : driver.getWindowHandles()) {	    	 
	        driver.switchTo().window(handle);
	      }   
	        driver.manage().window().maximize();
	        Thread.sleep(1000);
	        authentication();
	        getNameAndAge(name, year);
      
	
	}
	  @Test(priority=2)
	  public static void Diver_details() throws Exception{
		  authentication();
		  getDobAndAge(dob,age);
	  }
	  @Test(priority=2)
	  public static void Your_quote() {
	  
	  WebElement cost_monthly= Pom.monthly_payment(driver);
	  String estimated_cost=cost_monthly.getText().toString();
	  System.out.println("estimated_cost: "+estimated_cost);
	  logger = extent.createTest("estimated_cost", "estimated_cost");
	  logger.log(Status.PASS, MarkupHelper.createLabel("Test Case Passed is passTest", ExtentColor.GREEN));
	  WebElement Premium=Pom.Premium(driver);
	  String premium=Premium.getText().toString();
	  System.out.println("premium: "+premium);
	  
	  try {
		UpdateExcel.updateEx(String.valueOf(estimated_cost),String.valueOf(premium));
	} catch (Exception e) {
		e.printStackTrace();
	}
	  
	  }
	  
	  public static Object[][] authentication() throws Exception{
		  ExcelUtils excel=new ExcelUtils();
		     Object[][] testObjArray =excel.getTableArray("E:\\MavenProject\\src\\main\\java\\testdata\\Book1.xlsx", "Sheet1");
		     
		     System.out.println(" inputs are "+testObjArray);
		     for (Object[] objects : testObjArray) {
		    	 for(int i=0;i<objects.length;i++) {
		    		 name = (String) objects[0];
		    		 year = (String) objects[1];
		    		 dob = (String) objects[2];
		    		 age = (String) objects[3];
		    	 }
				
			}
		     
		     return (testObjArray);

			}
	 
	  public static void getNameAndAge(String name,String year) throws Exception{
		  WebElement radio=wait.until(ExpectedConditions.elementToBeClickable(Pom.comprehense_radiobutton(driver)));
			 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", radio);
			 radio.click();	 	
			  Select dropdown = new Select(Pom.Insurance_start_date(driver));
			  dropdown.selectByIndex(2);
			  WebElement input=wait.until(ExpectedConditions.elementToBeClickable(Pom.Street_address(driver)));
			  input.sendKeys(name);
			  Robot r=new Robot();
			    Thread.sleep(2000);
			    r.keyPress(KeyEvent.VK_DOWN);
			    r.keyRelease(KeyEvent.VK_DOWN);
			    r.keyPress(KeyEvent.VK_ENTER);    
			    r.keyRelease(KeyEvent.VK_ENTER); 
			  Thread.sleep(1000);
			  logger = extent.createTest("driverdaetails", "estimated_cost");
			  logger.log(Status.PASS, MarkupHelper.createLabel("diverdeatils", ExtentColor.GREEN));
			  Select dropdown2 = new Select(Pom.garaging(driver));
			  dropdown2.selectByVisibleText("Garage");
			  Pom.year(driver).sendKeys(year);
			  Thread.sleep(500);
			  Pom.make(driver).click();
			  WebElement option=Pom.make_option(driver)	;
			  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", option);
			  option.click();
			  Select modedropdown=new Select(Pom.model(driver));
			  modedropdown.selectByVisibleText("FORESTER");
			  Thread.sleep(500);
			  Select transmission= new Select(Pom.transmission(driver));
			  transmission.selectByIndex(1);
			  Thread.sleep(1000);
			  Pom.Vechile_list(driver).click();
			  Thread.sleep(500);
			  WebElement label_no=wait.until(ExpectedConditions.elementToBeClickable(Pom.additional_info_no(driver)));
			  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", label_no);
			  label_no.click();
			  Pom.accessories(driver).click();
			  Pom.modification(driver).click();
			  Select kilometer=new Select(Pom.addition_information(driver));
			  kilometer.selectByIndex(3);
			  WebElement next_button=wait.until(ExpectedConditions.elementToBeClickable(Pom.Next_button(driver)));
			  next_button.click();
	  }
	  
	  public static void getDobAndAge(String dob,String age) throws Exception {
		  WebElement DObvalue=wait.until(ExpectedConditions.elementToBeClickable(Pom.Dob(driver)));
		  DObvalue.sendKeys(dob);
		  Pom.Gender(driver).click();
		  Pom.age(driver).sendKeys(age);
		  Pom.owner(driver).click();
		  Pom.owner_of_registered_vechile(driver).click();
		  Pom.claims(driver).click();
		  Pom.under_25years(driver).click();
		  Select finanacedropdown= new Select(Pom.Vechile_financed(driver));
		  finanacedropdown.selectByVisibleText("No Finance");
		  Pom.vechile_used_for(driver).click();
		  Thread.sleep(500);	 
//		  WebElement status=wait.until(ExpectedConditions.elementToBeClickable());
		  Select status1=new Select(Pom.employee_status(driver));
		  status1.selectByIndex(1);
		  Select current_occupany=new Select(Pom.current_occupany(driver));
		  current_occupany.selectByVisibleText("Owner");
		  Pom.comphrensive_insurance(driver).click();
		  Pom.TC(driver).click();
		  Pom.doc(driver).click();
		  Thread.sleep(1000);
		  WebElement click_next=wait.until(ExpectedConditions.elementToBeClickable(Pom.Next_button2(driver)));
		  click_next.click();
		  Thread.sleep(3000);
	  }
	  @AfterSuite
	  public void endReport(){
	  	extent.flush();
	  }
	  
	  
	}
	


