package object_repository;

public class Object_Repository {
	
	public static String Car_insurance="//div[@class='container hidden-xs']/div[1]/div[2]/div[1]/div[1]/div[2]/ul[1]/li[1]/a";
	public static String Get_quote="//div[@class='mega-menu']/div/table/tbody/tr/td[3]/div/a";
	public static String comprehense_radiobutton="//div[@class='form-section'][2]/div[2]/div[2]/div/label/input[1]";
	public static String Insurance_start_date="//div[@class='form-section'][3]/div/following-sibling::div/div/div[2]/div/select";
	public static String Street_address="//div[@class='form-section'][4]/div[1]/following-sibling::div[1]/div[3]/div[2]/input";
	public static String garaging="//div[@class='form-section'][4]/div[3]/div[2]/select";
	public static String year="//div[@class='form-section'][5]/div[2]/div[3]/input";
	public static String make="//div[@class='form-section'][5]/div[3]/div[2]/select";
	public static String make_option="//div[@class='form-section'][5]/div[3]/div[2]/select/option[69]";
	public static String model="//div[@class='form-section'][5]/div[4]/div[2]/select";
	public static String transmission="//div[@class='form-section'][5]/div[5]/div[2]/select";
	public static String addition_information="//div[@class='form-section'][8]/div[5]/div[2]/select";
	public static String Next_button="//div[@class='page-nav-button-container']/input";
	public static String Vechile_list="//div[@class='form-section'][5]/div[7]/div[2]/div[2]/div[1]/input";
	public static String additional_info_no="//div[@class='form-section'][7]/div[2]/div[2]/div/label[2]";
	public static String accessories="//div[@class='form-section'][7]/div[4]/div[2]/div/label[2]";
	public static String modification="//div[@class='form-section'][7]/div[6]/div[2]/div/label[2]";
	public static String Dob="//div[@class='form-section drivers-section']/div/div[3]/div[2]/input[2]";
	public static String Gender="//div[@class='form-section drivers-section']/div/div[4]/div[2]/div/label[2]";
	public static String age="//div[@class='form-section drivers-section']/div/div[5]/div[2]/input";
	public static String owner="//div[@class='form-section drivers-section']/div/div[7]/div[2]/div/label[1]";
	public static String owner_of_registered_vechile="//div[@class='form-section drivers-section']/div/div[8]/div[2]/div/label[1]";
	public static String claims="//div[@class='form-section drivers-section']/div/div[9]/div[4]/div/label[2]";
	public static String under_25years="//div[@class='form-section'][1]/div[2]/div[2]/div/label[1]";
	public static String Vechile_financed="//div[@class='form-section'][1]/div[3]/div[2]/select";
	public static String vechile_used_for="//div[@class='form-section'][1]/div[5]/div[2]/div/label[1]";
	public static String employee_status="//div[@class='form-section'][1]/div[7]/div[1]/div[2]/select";
	public static String comphrensive_insurance="//div[@class='form-section'][1]/div[8]/div[2]/div/label[2]";
	public static String  current_occupany="//div[@class='form-section'][1]/div[7]/div[2]/div[2]/select";
	public static String TC="//div[@class='form-section'][4]/div[2]/div/div/div/span/input";
	public static String doc="//div[@class='form-section'][4]/div[3]/div/div/div/span/input";
	public static String Next_button2="//div[@class='page-nav-button-container']/input[1]";
	public static String monthly_payment="//div[@class='col-xs-24 col-sm-7'][1]/div[1]/div[2]/p";
	public static String Premium="//div[@class='col-xs-24 col-sm-7'][1]/div[1]/div[2]/p[2]";
	
	

}
