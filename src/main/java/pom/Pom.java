package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import object_repository.Object_Repository;

public class Pom {
	public static WebElement driver = null;

	public static WebElement Car_insurance(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.Car_insurance));
		return element;
	}

	public static WebElement Get_quote(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.Get_quote));
		return element;
	}
	public static WebElement comprehense_radiobutton(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.comprehense_radiobutton));
		return element;
	}
	public static WebElement Insurance_start_date(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.Insurance_start_date));
		return element;
	}
	public static WebElement Street_address(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.Street_address));
		return element;
	}
	public static WebElement garaging(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.garaging));
		return element;
	}
	public static WebElement year(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.year));
		return element;
	}
	public static WebElement make(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.make));
		return element;
	}
	public static WebElement model(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.model));
		return element;
	}
	public static WebElement transmission(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.transmission));
		return element;
	}
	public static WebElement addition_information(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.addition_information));
		return element;
	}
	public static WebElement Next_button(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.Next_button));
		return element;
	}
	public static WebElement make_option(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.make_option));
		return element;
	}
	public static WebElement Vechile_list(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.Vechile_list));
		return element;
	}
	public static WebElement additional_info_no(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.additional_info_no));
		return element;
	}
	public static WebElement accessories(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.accessories));
		return element;
	}
	public static WebElement modification(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.modification));
		return element;
	}
	public static WebElement Dob(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.Dob));
		return element;
	}
	public static WebElement Gender(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.Gender));
		return element;
	}
	public static WebElement age(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.age));
		return element;
	}
	public static WebElement owner(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.owner));
		return element;
	}
	public static WebElement owner_of_registered_vechile(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.owner_of_registered_vechile));
		return element;
	}
	public static WebElement claims(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.claims));
		return element;
	}
	public static WebElement Vechile_financed(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.Vechile_financed));
		return element;
	}
	public static WebElement vechile_used_for(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.vechile_used_for));
		return element;
	}
	public static WebElement comphrensive_insurance(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.comphrensive_insurance));
		return element;
	} 
	public static WebElement employee_status(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.employee_status));
		return element;
	}
	public static WebElement current_occupany(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.current_occupany));
		return element;
	}
	public static WebElement TC(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.TC));
		return element;
	}
	public static WebElement doc(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.doc));
		return element;
	}
	public static WebElement Next_button2(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.Next_button2));
		return element;
	}
	public static WebElement monthly_payment(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.monthly_payment));
		return element;
	}
	public static WebElement under_25years(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.under_25years));
		return element;
	}
	public static WebElement Premium(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(Object_Repository.Premium));
		return element;
	}
	
	
} 
